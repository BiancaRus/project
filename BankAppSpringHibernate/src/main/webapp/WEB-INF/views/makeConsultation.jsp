<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

input[type=text]:focus {
	border-color: white;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	cursor: pointer;
	color: white;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Update Consultation</title>
</head>
<body>

	<ul>
		<li><a href="/SpringHibernateExample/homeDoctor">Home</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>

	<center>
		<h2>Update Consultation</h2>

		<form:form modelAttribute="consultation"
			action="/SpringHibernateExample/makeConsultation/${consultation.consultationId}"
			method="POST">
			<table>
				<tr>
					<td>Patient Name</td>
					<td><c:out
							value="${consultation.patient.firstName}  ${consultation.patient.lastName}" /></td>
				</tr>
				<tr>
					<td>Description</td>
					<td><form:input path="description" /></td>
					<td><form:errors path="description" cssClass="error" /></td>
				</tr>
				<tr>
					<td>Diagnosis</td>
					<td><form:input path="diagnosis" /></td>
					<td><form:errors path="description" cssClass="error" /></td>
				</tr>

				<tr>
					<td></td>
					<td><input type="submit" value="Edit" /></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>