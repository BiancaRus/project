<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Login Failed</title>
</head>
<body bgcolor="#C0C0C0">
	<center>
		<h2>Wrong password or User name.</h2>
		<h1>
			Please <a href="/SpringHibernateExample/" style="color: #400000;">Try
				again!</a>
		</h1>
	</center>
</body>
</html>