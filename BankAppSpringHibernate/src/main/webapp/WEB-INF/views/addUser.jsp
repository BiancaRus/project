
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style type="text/css">
ul {
	list-style-type: none;
	margin: 0;
	padding: 10px;
	width: 200px;
	background-color: #f1f1f1;
}

li a {
	display: block;
	color: #000;
	padding: 8px 0 8px 16px;
	text-decoration: none;
}

li a:hover {
	background-color: #555;
	color: white;
}

h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
	background-color: #C0C0C0;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

input[type=text]:focus {
	border-color: white;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	color: white;
	cursor: pointer;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>addUser</title>
</head>
<body>

	<ul>
		<li><a href="/SpringHibernateExample/usersList">Users List</a></li>
		<li><a href="/SpringHibernateExample/">Log Out</a></li>
	</ul>


	<center>

		<h2>Clinic New Employee Registration</h2>


		<form:form commandName="user" action="/SpringHibernateExample/create/"
			method="POST">
			<table>
				<tr>
					<td>User name</td>
					<td><form:input path="username" /></td>
					<td><form:errors path="username" cssClass="error" /></td>
				</tr>
				<tr>
					<td>First Name</td>
					<td><form:input path="userFirstName" /></td>

				</tr>

				<tr>
					<td>Last Name</td>
					<td><form:input path="userLastName" /></td>
				</tr>
				<tr>
					<td>Password</td>
					<td><form:input path="password" /></td>
					<td><form:errors path="password" cssClass="error" /></td>

				</tr>
				<tr>
					<td>Email</td>
					<td><form:input path="userEmail" /></td>
					<td><form:errors path="userEmail" cssClass="error" /></td>
				</tr>
				<tr>
					<td>Role</td>
					<td><form:select path="role">
							<option value="1">Administrator</option>
							<option value="2">Doctor</option>
							<option value="3">Clinic Secretary</option>
						</form:select></td>
				</tr>


				<tr>
					<td></td>
					<td><input type="submit" value="Register" /></td>
				</tr>
			</table>
		</form:form>
	</center>
</body>
</html>