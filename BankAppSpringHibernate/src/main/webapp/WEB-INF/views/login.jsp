<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
	<%@page session="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<style type="text/css">
h2 {
	color: #505050;
	font-family: sans-comic;
	font-size: 300%;
}

h1 {
	color: #505050;
	font-family: sans-comic;
	font-size: 200%;
	paddingRight: 10px;
}

body {
	padding: 20px;
}

input[type=text] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

input[type=text]:focus {
	border-color: white;
}

input[type=submit] {
	padding: 5px 15px;
	background: #900000;
	border: 0 none;
	cursor: pointer;
	color: white;
	-webkit-border-radius: 5px;
	border-radius: 5px;
}

input[type=password] {
	padding: 5px;
	border: 2px solid #ccc;
	-webkit-border-radius: 5px;
	border-radius: 5px;
	border: 2px solid #ccc;
}
</style>
<title>AmbasadorLogin</title>
</head>
<body bgcolor="#C0C0C0">

	<br>

	<Center>
		<h2>Welcome to Ambasador Restaurant Order application!</h2>
	</Center>
	<br>
	<br>
	<br>

	<h1>Log In</h1>
	<br>
	<form:form name="loginForm" commandName="user"
		action="/SpringHibernateExample/authenticate/" method="POST">
		<table>
			<tr>
				<td>User name</td>
				<td><form:input path="username" /></td>

			</tr>
			<tr>
				<td>Password</td>
				<td><form:password path="password" /></td>

			</tr>

			<tr>
				<td></td>
				<td><input type="submit" value="Log In" /></td>
			</tr>
		</table>
	</form:form>

<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>

</body>
</html>
