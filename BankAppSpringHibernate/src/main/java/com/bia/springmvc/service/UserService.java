package com.bia.springmvc.service;

import java.util.List;

import com.bia.springmvc.model.User;

public interface UserService {
	public void addUser(User user);

	public void updateUser(User user);

	public User getUserById(int id);

	public void deleteUser(int id);

	public List<User> getUsers();

	public List<User> getEmployees();

	public boolean isUnique(String username);
}
