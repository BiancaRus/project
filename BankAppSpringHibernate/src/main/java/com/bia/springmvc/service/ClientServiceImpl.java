package com.bia.springmvc.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bia.springmvc.dao.ClientDao;
import com.bia.springmvc.model.Client;

@Service("clientService")
@Transactional
public class ClientServiceImpl implements ClientService {
	@Autowired
	ClientDao clientdao;

	public void addClient(Client client) {
		clientdao.addClient(client);
	}

	public void updateClient(Client client) {
		clientdao.updateClient(client);
	}

	public Client getClient(int clientId) {
		return clientdao.getClient(clientId);
	}

	public void deleteClient(int clientId) {
		clientdao.deleteClient(clientId);

	}

	public List<Client> getClients() {
		return clientdao.getClients();
	}

	public boolean isUnique(String cnp) {
		return clientdao.isUnique(cnp);
	}
}
