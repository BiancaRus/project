package com.bia.springmvc.service;

import java.util.List;

import com.bia.springmvc.model.Client;

public interface ClientService {

	public void addClient(Client client);

	public void updateClient(Client client);

	public Client getClient(int clientId);

	public void deleteClient(int clientId);

	public List<Client> getClients();

	public boolean isUnique(String cnp);
}