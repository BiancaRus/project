package com.bia.springmvc.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.bia.springmvc.model.Order;

@Repository("orderDao")
public class OrderDaoImpl extends AbstractDao<Integer, Order> implements OrderDao {

	public void addConsultation(Order consultation) {
		persist(consultation);

	}

	@SuppressWarnings("unchecked")
	public List<Order> getAllConsultations() {
		Criteria criteria = createEntityCriteria();
		return criteria.list();
	}

	@SuppressWarnings("unchecked")
	public List<Order> getConsultationByPatient(int patient) {
		Criteria criteria = createEntityCriteria();
		criteria.add(Restrictions.eq("id_client", patient));
		return criteria.list();
	}

	public void updateConsultation(Order consultation) {
		Order consultationUpdated = getConsultationById(consultation.getConsultationId());

		consultationUpdated.setDate(consultation.getDate());
		consultationUpdated.setDescription(consultation.getDescription());
		consultationUpdated.setDiagnosis(consultation.getDiagnosis());
		consultationUpdated.setDoctor(consultation.getDoctor());
		consultationUpdated.setPatient(consultation.getPatient());
		consultationUpdated.setIsServed(consultation.getIsServed());
		update(consultationUpdated);

	}

	public void deleteConsultation(int id) {
		delete(getByKey(id));

	}

	public Order getConsultationById(int id) {
		return getByKey(id);
	}

}