package com.bia.springmvc.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "consultation")
public class Order {

	@Id
	@GeneratedValue
	@Column(name = "idorder")
	private int consultationId;

	@OneToOne
	@JoinColumn(name = "id_client")
	private Client patient;

	@OneToOne
	@JoinColumn(name = "id_waiter")
	private User doctor;

	@Column(name = "order_description")
	@Size(min = 1)
	private String description;

	@Column(name = "food_quantity")
	private String diagnosis;

	@Column(name = "order_date")
	private Date date;

	@Column(name = "table_number")
	private int tableNumber;
	
	@Column(name = "total_cost")
	private int totalCost;

	public int getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(int totalCost) {
		this.totalCost = totalCost;
	}

	public int getIsServed() {
		return isServed;
	}

	public void setIsServed(int isServed) {
		this.isServed = isServed;
	}

	@Column(name = "served")
	private int isServed;

	public int getTableNumber() {
		return tableNumber;
	}

	public void setTableNumber(int tableNumber) {
		this.tableNumber = tableNumber;
	}

	public int getConsultationId() {
		return consultationId;
	}

	public void setConsultationId(int consultationId) {
		this.consultationId = consultationId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDiagnosis() {
		return diagnosis;
	}

	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getDoctor() {
		return doctor;
	}

	public void setDoctor(User doctor) {
		this.doctor = doctor;
	}

	public Client getPatient() {
		return patient;
	}

	public void setPatient(Client patient) {
		this.patient = patient;
	}

}
