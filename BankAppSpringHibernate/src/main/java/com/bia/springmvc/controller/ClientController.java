package com.bia.springmvc.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bia.springmvc.model.Client;
import com.bia.springmvc.service.ClientService;

@Controller
@RequestMapping("/")
public class ClientController {

	@Autowired
	private ClientService clientService;

	@RequestMapping(value = "/createClient", method = RequestMethod.GET)
	public ModelAndView showClientForm() {
		ModelAndView modelAndView = new ModelAndView("createClient");

		modelAndView.addObject("client", new Client());

		return modelAndView;
	}

	@RequestMapping(value = "/createClient", method = RequestMethod.POST)
	public String saveClient(Model model, @ModelAttribute @Valid Client client, BindingResult result) {

		ModelAndView modelAndView = new ModelAndView("redirect:/clientsList");
		if (result.hasErrors() || !clientService.isUnique(client.getPersonalNumericCode())) {
			return "createClient";
		}
		clientService.addClient(client);

		String message = "Client was successfully added.";
		modelAndView.addObject("message", message);

		return "redirect:/clientsList";
	}

	@RequestMapping(value = "/clientsList", method = RequestMethod.GET)
	public ModelAndView list(Model model) {
		ModelAndView modelAndView = new ModelAndView("viewClients");

		List<Client> clients = clientService.getClients();
		modelAndView.addObject("clients", clients);

		return modelAndView;
	}

	@RequestMapping(value = "/deleteClient/{idclient}", method = RequestMethod.GET)
	public ModelAndView deleteClient(@PathVariable int idclient) {

		ModelAndView modelAndView = new ModelAndView("redirect:/clientsList");
		clientService.deleteClient(idclient);
		String message = "Client was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value = "/editClient/{idclient}", method = RequestMethod.GET)
	public ModelAndView updateClient(@PathVariable Integer idclient) {
		ModelAndView modelAndView = new ModelAndView("updateClient");

		Client client = clientService.getClient(idclient);
		modelAndView.addObject("client", client);

		return modelAndView;
	}

	@RequestMapping(value = "/editClient/{idclient}", method = RequestMethod.POST)
	public String updateClient(@ModelAttribute("client") @Valid Client client, BindingResult result,
			@PathVariable int idclient, ModelMap model) {
		if (result.hasErrors()) {
			client.setClientId(idclient);
			model.addAttribute("client", client);
			return "updateClient";
		}

		client.setClientId(idclient);
		clientService.updateClient(client);
		return "redirect:/clientsList";
	}
}
