package com.bia.springmvc.controller;

import java.util.ArrayList;
import java.util.Date;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bia.springmvc.model.Client;
import com.bia.springmvc.model.Order;
import com.bia.springmvc.model.User;
import com.bia.springmvc.service.ClientService;
import com.bia.springmvc.service.ConsultationService;
import com.bia.springmvc.service.UserService;

@Controller
@RequestMapping("/")
public class BillController {

	@Autowired
	private ConsultationService consultationService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private UserService userService;

	@RequestMapping(value = "/createBill", method = RequestMethod.GET)
	public ModelAndView saveBill() {

		ModelAndView modelAndView = new ModelAndView("addBill");

		modelAndView.addObject("doctors", userService.getEmployees());
		modelAndView.addObject("patients", clientService.getClients());
		ArrayList<Integer> numbers = new ArrayList<Integer>();
		for (int i = 0; i < 10; i++) {
			numbers.add(i + 1);
		}
		modelAndView.addObject("numbers", numbers);
		modelAndView.addObject("consultation", new Order());

		return modelAndView;
	}

	@RequestMapping(value = "/createBill", method = RequestMethod.POST)
	public String addBill(@ModelAttribute @Valid Order consultation, BindingResult result, ModelMap model) {
		if (result.hasErrors()) {
			return "addBill";
		}
		User user = LoginController.getUser();
		System.out.println("from here now" + user.getUserFirstName());
		consultation.setDoctor(user);

		consultation.setDate(new Date());
		consultationService.addConsultation(consultation);
		return "redirect:/billsList";
	}

	@RequestMapping(value = "/billsList", method = RequestMethod.GET)
	public ModelAndView list(Model model) {
		ModelAndView modelAndView = new ModelAndView("viewBills");

		modelAndView.addObject("consultations", consultationService.getAllConsultations());
		modelAndView.addObject("user", LoginController.getUser());

		return modelAndView;
	}

	@RequestMapping(value = "/homeDoctor", method = RequestMethod.GET)
	public ModelAndView doctorList(Model model) {
		ModelAndView modelAndView = new ModelAndView("doctorHome");

		modelAndView.addObject("consultations", consultationService.getAllConsultations());
		modelAndView.addObject("user", LoginController.getUser());

		return modelAndView;
	}

	@RequestMapping(value = "/deleteConsultation/{idconsultation}", method = RequestMethod.GET)
	public ModelAndView deleteConsultation(@PathVariable int idconsultation) {

		ModelAndView modelAndView = new ModelAndView("redirect:/billsList");
		consultationService.deleteConsultation(idconsultation);
		String message = "Consultation was successfully deleted.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value = "/serveOrder/{idconsultation}", method = RequestMethod.GET)
	public ModelAndView serveConsultation(@PathVariable int idconsultation) {

		ModelAndView modelAndView = new ModelAndView("doctorHome");
		modelAndView.addObject("consultations", consultationService.getAllConsultations());
		modelAndView.addObject("user", LoginController.getUser());
		Order order = consultationService.getConsultationById(idconsultation);
		if (order.getIsServed() == 0) {
			order.setIsServed(1);
			consultationService.updateConsultation(order);
		}
		String message = "Consultation was successfully update.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value = "/processBill/{idconsultation}", method = RequestMethod.GET)
	public ModelAndView processBill(@PathVariable int idconsultation) {

		ModelAndView modelAndView = new ModelAndView("redirect:/billsList");
		Order order = consultationService.getConsultationById(idconsultation);
		if (order.getIsServed() == 1) {
			Client client = clientService.getClient(order.getPatient().getClientId());
			try {
				if (client.getSum() - order.getTotalCost() > 0)
					client.setSum(client.getSum() - order.getTotalCost());
				clientService.updateClient(client);
				order.setIsServed(2);
				consultationService.updateConsultation(order);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}
		// consultationService.updateConsultation(order);}
		String message = "Consultation was successfully update.";
		modelAndView.addObject("message", message);
		return modelAndView;
	}

	@RequestMapping(value = "/editConsultation/{idconsultation}", method = RequestMethod.GET)
	public ModelAndView updateUser(@PathVariable int idconsultation) {
		ModelAndView modelAndView = new ModelAndView("updateConsultation");
		Order consultation = consultationService.getConsultationById(idconsultation);
		modelAndView.addObject("doctors", userService.getEmployees());
		modelAndView.addObject("patients", clientService.getClients());
		modelAndView.addObject("consultation", consultation);
		return modelAndView;
	}

	@RequestMapping(value = "/editConsultation/{idconsultation}", method = RequestMethod.POST)
	public String updateUser(@ModelAttribute("consultation") @Valid Order consultation, BindingResult result,
			@PathVariable int idconsultation, ModelMap model) {
		if (result.hasErrors()) {
			consultation.setConsultationId(idconsultation);
			model.addAttribute("doctors", userService.getEmployees());
			model.addAttribute("patients", clientService.getClients());
			model.addAttribute("consultation", consultation);
			return "updateConsultation";
		}
		consultation.setConsultationId(consultation.getTableNumber());
		consultation.setDoctor(LoginController.getUser());
		consultation.setConsultationId(idconsultation);
		consultationService.updateConsultation(consultation);
		return "redirect:/billsList";
	}

	Client aux;

	@RequestMapping(value = "/makeConsultation/{idconsultation}", method = RequestMethod.GET)
	public ModelAndView makeConsultation(@PathVariable int idconsultation) {
		ModelAndView modelAndView = new ModelAndView("makeConsultation");
		Order consultation = consultationService.getConsultationById(idconsultation);
		aux = consultation.getPatient();
		modelAndView.addObject("consultation", consultation);
		return modelAndView;
	}

	@RequestMapping(value = "/makeConsultation/{idconsultation}", method = RequestMethod.POST)
	public String makeConsultation(@ModelAttribute("consultation") @Valid Order consultation, BindingResult result,
			@PathVariable int idconsultation, ModelMap model) {
		if (result.hasErrors()) {
			consultation.setConsultationId(idconsultation);
			model.addAttribute("consultation", consultation);
			return "makeConsultation";
		}
		consultation.setDate(new Date());
		consultation.setDoctor(LoginController.getUser());
		consultation.setPatient(aux);
		consultation.setConsultationId(idconsultation);
		consultationService.updateConsultation(consultation);
		return "redirect:/homeDoctor";
	}

}
