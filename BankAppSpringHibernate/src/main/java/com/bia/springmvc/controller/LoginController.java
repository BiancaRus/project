package com.bia.springmvc.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import com.bia.springmvc.model.Client;
import com.bia.springmvc.model.User;
import com.bia.springmvc.service.ClientService;
import com.bia.springmvc.service.ConsultationService;
import com.bia.springmvc.service.UserService;

@Controller
@RequestMapping("/")
public class LoginController {

	@Autowired
	private UserService userService;
	@Autowired
	private ClientService clientService;
	@Autowired
	private ConsultationService consultationService;

	public static User loggedUser = new User();

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public ModelAndView loginAttempt() {
		ModelAndView modelAndView = new ModelAndView("login");
		modelAndView.addObject("user", new User());

		return modelAndView;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ModelAndView loginForm(@ModelAttribute("user") User user, HttpServletRequest request) {
		ModelAndView modelAndView;
		
		for (User findUser : userService.getUsers()) {
			if (findUser.getUsername().equals(user.getUsername())
					&& findUser.getPassword().equals(user.getPassword())) {
				List<Client> clients = clientService.getClients();
				setUser(findUser);
				request.getSession().setAttribute("blabla", user.getUsername());
				if (findUser.getRole() == 1) {
					modelAndView = new ModelAndView("adminHome");

					modelAndView.addObject("user", findUser);

					modelAndView.addObject("clients", clients);
					return modelAndView;
				} else if (findUser.getRole() == 2) {
					modelAndView = new ModelAndView("doctorHome");

					modelAndView.addObject("user", findUser);

					modelAndView.addObject("consultations", consultationService.getAllConsultations());
					
					return modelAndView;

				} else if (findUser.getRole() == 3) {
					modelAndView = new ModelAndView("viewClients");

					modelAndView.addObject("user", findUser);

					modelAndView.addObject("clients", clients);
					
					return modelAndView;

				}
			}else{
				request.getSession().setAttribute("blabla", "");
			}
		}

		modelAndView = new ModelAndView("redirect:/loginFailed");

		return modelAndView;
	}

	@RequestMapping(value = "/loginFailed", method = RequestMethod.GET)
	public ModelAndView loginerror(User user) {
		ModelAndView modelAndView;

		modelAndView = new ModelAndView("loginFailed");

		return modelAndView;

	}

	public static void setUser(User user) {
		loggedUser = user;
	}

	public static User getUser() {
		return loggedUser;
	}

}