package com.bia.springmvc.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.bia.springmvc.model.Order;
import com.bia.springmvc.service.ConsultationService;

@RestController
public class GreetingController {

  
    @Autowired 
    ConsultationService consultationService;

    @RequestMapping("/greeting")
    public List<Order> greeting(@RequestParam(value="name", defaultValue="World") String name) {
        return consultationService.getAllConsultations();
    }
}